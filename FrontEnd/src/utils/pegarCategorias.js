import { getAllCategorias } from "../services/categoriaService";

const pegarCategorias = async (configurarState) => {
   
    const listaCategorias = (await getAllCategorias()).data;

    listaCategorias.sort((a, b) => {
        return (a.categoriaDescription > b.categoriaDescription) ? 1 : -1;
    } )
    

  configurarState(listaCategorias);
};


export default pegarCategorias