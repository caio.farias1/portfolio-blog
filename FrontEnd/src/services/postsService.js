import axios from '../axios/axios';

const transformQuery = (query) => {
    let resultado = ""
    if(query.idCategoria){
        resultado += `/?categoria=${query.idCategoria}`
    }
    return resultado;
}

export const getAllPosts = (queryParams) => axios.get('/pics'+transformQuery(queryParams));

export const getPostById = ( id ) => axios.get(`/pics/${id}`);

export const deletePostById = ( id ) => axios.delete(`/pics/${id}`);
 
