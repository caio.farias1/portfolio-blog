import axios from '../axios/axios';


export const getAllCategorias = () => axios.get('/categorias');

export const deleteCategoriaById = (idCategoria) => axios.delete(`/categorias/${idCategoria}`);

export const createCategoria = (data) => axios.post('/categorias', data);

 
