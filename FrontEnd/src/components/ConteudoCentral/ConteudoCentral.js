import React, {useState, useEffect} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import './ConteudoCentral.css';
import ListaPosts from './ListaPosts/ListaPosts';
import ListaCategorias from './ListaCategorias/ListaCategorias';
import NovoPost from './NovoPost/NovoPost';
import Post from './ListaPosts/Post/Post'
import DetalhesPost from './DetalhesPost/DetalhesPost'
import PostsPorCategoria from './PostsPorCategoria/PostsPorCategoria'
import pegarCategorias from '../../utils/pegarCategorias'
import pegarPosts from '../../utils/pegarPosts'
import BarraLateral from '../BarraLateral/BarraLateral';


const ConteudoCentral = () => {
    
    const [ categorias, setCategorias] = useState([]);

    const getCategorias = () =>{
        pegarCategorias( setCategorias );
    }

    useEffect( () => {
        getCategorias()
    }, []);

    return (
        <main>
           
            <Switch>

                {/* Redirecionamento ROOT */}
                <Route exact path="/">
                    <Redirect to="/lista-posts"/>
                </Route>

                <Route path="/lista-posts/:idCategoria"><ListaPosts/></Route>    
                <Route path="/lista-posts"><ListaPosts/></Route>

                <Route path="/lista-categorias"><ListaCategorias lista = { categorias} getCategorias = {getCategorias} /></Route>
                <Route path="/novo-post"><NovoPost lista = { categorias} /></Route>

                <Route path="/post/:id"><DetalhesPost/></Route>
 
                
            </Switch>
            <BarraLateral lista = { categorias}/>

        </main>
    )
};

export default ConteudoCentral;