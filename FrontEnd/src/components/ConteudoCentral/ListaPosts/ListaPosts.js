
import { GridList, GridListTile} from '@material-ui/core';
import React, {useState, useContext, useEffect} from 'react';
import './ListaPosts.css'
import Post from './Post/Post';
import { useParams } from 'react-router';
import {getAllPosts} from '../../../services/postsService';
import {Link, useHistory} from 'react-router-dom';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import IconButton from '@material-ui/core/IconButton';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';



import TemaContext from '../../../contexts/TemaContexts'
import { identity } from 'lodash';


const ListaPosts = () => {
    const history = useHistory();

    const {idCategoria} = useParams();
    const historyTest = (id) => {
        history.push(`/post/${id}`)
    }

    const tema = useContext(TemaContext);

    const [lista, setLista ] = useState([]);

    const [listaCurtida, setListaCurtida] = useState([]);

    const listaPosts = async () => {
        const resultado = await getAllPosts({idCategoria});
        const _listaPosts = resultado.data
        setLista(_listaPosts);
    };

    useEffect(() => {
        listaPosts()
    }, [idCategoria]);

    const toggleLike = (id) => {
        const indexCurtida = listaCurtida.indexOf(id);
        let novaListaCurtida = [...listaCurtida]
        
        const indexPostCurtido = lista.findIndex((item) => {
            return item.id === id
        })
        let _lista = [...lista]

        if (indexCurtida > -1) {
            novaListaCurtida.splice(indexCurtida, 1)
            _lista[indexPostCurtido].likes -= 1;
        } else {
            novaListaCurtida.push(id);
            _lista[indexPostCurtido].likes += 1;
        }
        setListaCurtida(novaListaCurtida);
        setLista(_lista);


    };


    return (
        <>
        
        <div className="posts">
                {lista.map((item) => (
                     <Grid item xs={3}>
                    <div>
                        <Link to={`/post/${item.id}`}>
                            <div className="likes"> 
                                <img className="img-post" src={item.image} alt={item.title}/>
                            </div>     
                        </Link>
                        <div>
                            <IconButton onClick = {() => toggleLike(item.id)}>
                                {listaCurtida.indexOf(item.id) > -1 ? <FavoriteIcon/> : <FavoriteBorderIcon/>} 
                            </IconButton>{item.likes}

                        </div>
                    </div>
                    </Grid>
            ))}
        </div>
        </>
    );
};

export default ListaPosts;