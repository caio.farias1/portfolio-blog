import React, { useEffect, useState }  from 'react';
import { useHistory, useParams } from 'react-router-dom'
import './DetalhesPost.css'
import {getPostById} from '../../../services/postsService'
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import {deletePostById} from '../../../services/postsService';

const Post = () => {

    const { id } = useParams();
    const history = useHistory();

    const [postDetail, setPostDetail] = useState({});
    
    const detalhesPost = async () => {
        const _postDetail = await getPostById(id);
        setPostDetail(_postDetail.data)
    }

    const deletePost = async () => {
        const _deletePost = await deletePostById(id);
        if (_deletePost.status === 200) {
            history.push('/');
        }
    }
    
    console.log(postDetail);

    useEffect(() => {  
    detalhesPost()
    }, [id]);

    // if (Object.keys(postDetail).length === 0){
    //     return (
    //         <div><p>Dados não encontrados!</p></div>
    //     )
    // } 
            

            
    return (

        <article>
            <div className = "card">
                <img src = {postDetail.image} ></img>
                <div className = "likes">
                    <FavoriteBorderIcon/><p>{postDetail.likes}</p>
                </div>
                <div className='p-categoria'>
                    Categoria: {postDetail.categoriaDescription}
                </div>
                <p>{postDetail.description}</p>
            </div>

            {console.log(postDetail)}

            <button onClick={deletePost}>Remover</button>
        </article>
        
    )
};

export default Post;