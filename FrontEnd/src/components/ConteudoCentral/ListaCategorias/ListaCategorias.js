import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import './ListaCategorias.css';
import { createCategoria, deleteCategoriaById } from '../../../services/categoriaService';
import { result } from 'lodash';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faCoffee } from '@fortawesome/free-solid-svg-icons'

const ListaCategorias = ({ lista, getCategorias }) => {

    // const element = <FontAwesomeIcon icon={faCoffee} />
    const [description, setDescription] = useState('')
    const [mensagemRequest, setMensagemRequest] = useState('');

    const showMensagemRequest = (texto) => {
        setMensagemRequest(texto)
        setTimeout(()=>{setMensagemRequest('')}, 5000);
    }

    const createNovaCategoria = async evento => {
        console.log("Categoria submitado")
        evento.preventDefault();
        const novaCategoria = {
            "description": `#${description}`
        };
    

        const result = await createCategoria(novaCategoria);
        console.log(result);
        setDescription("");
        showMensagemRequest(result.status === 200 ? "Categoria criada com sucesso" : "Erro ao criar categoria.")

        getCategorias();
    }

    const removerCategoria = async (id) => {
        let texto = "Categoria excluida com sucesso!"
        try { 
            await deleteCategoriaById(id);    
        } catch (error) {
            console.log(error.response)
            if (error.response.status === 400){
                texto = "Categoria associada a post existente."
            } else {
                texto = "Erro ao excluir categoria!"
            };
        }
        
        showMensagemRequest(texto);
        getCategorias();
    }

    return (
        <div id="lista-categorias">
            <h2>Lista Categorias</h2>
            <p>{mensagemRequest}</p>

         
            <form onSubmit={createNovaCategoria}>
                <input type="text" 
                        name="nova-categoria" 
                        required
                        value ={description} 
                        onChange = {evento => setDescription(evento.target.value)} 
                        ></input>
                        <button>Nova categoria</button>
            </form>
           

            <div>
                <ul>
                    { lista.map( (item) => 
                        <div key={item.id} className = "categoria-linha">
                            <li>
                                <Link to = {`/lista-posts/${item.id}`}> {item.categoriaDescription}</Link>
                            </li>
                            <button onClick = { () => {removerCategoria(item.id)}}>Remover</button>
                            {/* {element} */}
                        </div>
                    )}


                    
                </ul>  
            </div>
         
        </div>
    )
};

export default ListaCategorias;