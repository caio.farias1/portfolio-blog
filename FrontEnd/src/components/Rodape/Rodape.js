import React, { useContext } from 'react';
import "./Rodape.css"
import TemaContext from '../../contexts/TemaContexts'

const Rodape = () => {

    const tema = useContext(TemaContext);

    return (
        <footer style={ { backgroundColor: tema.corFundoTema, color: tema.corLetra}}>
            <p>Copyright &#169; Caio Farias</p>
        </footer>
    )
};

export default Rodape;