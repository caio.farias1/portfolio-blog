import React, { useState, useEffect, useContext } from 'react';
import {Link} from "react-router-dom";
import './BarraLateral.css';
import Busca from './Busca/Busca'
import pegarCategorias from '../../utils/pegarCategorias'
import SearchIcon from '@material-ui/icons/Search';

import TemaContext from '../../contexts/TemaContexts'


const BarraLateral = ({lista}) => {
    
    var tema = useContext(TemaContext);



    return (
        <aside style={ { backgroundColor: tema.corFundoTema, color: tema.corLetra}}>
            
            <ul className= "lista-barra-lateral">
                { lista.map( (item) => 
                    <li key={item.id} >
                        <Link to = {`/lista-posts/${item.id}`}> {item.categoriaDescription}</Link>
                    </li>
                )}
                <li><SearchIcon/></li>
            </ul>
            <hr></hr>  
        </aside>
    )
};

export default BarraLateral;