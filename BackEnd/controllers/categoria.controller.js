const Categoria = require("../models/categoria.model");


// Retrieve all Pics from the database.
exports.findAll = (req, res) => {
    const filter = {}
    if(req.query && req.query.categoria){
      filter.idCategoria = req.query.categoria
    }
    Categoria.getAll(res, filter);
  };

  // Create and Save a new Pic
exports.create = (req, res) => {
    const {description} = req.body
    const newCategoria = {categoriaDescription:description}
    Categoria.create(newCategoria, res);
  };

  exports.delete = (req, res) => {
    const {idCategoria} = req.params
    Categoria.remove(idCategoria, res)
  };
  