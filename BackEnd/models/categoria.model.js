const sql = require("./db.js");

// constructor
const Categoria = function(categoria) {
    this.id = categoria.id;
    this.description = categoria.description;
  };

  Categoria.getAll = (response, filter) => {
    let query = "SELECT * FROM categoria"
    let values = [];
    if (filter.id) {
      values.push(filter.id)
      query += " WHERE id = ?"  
    }
    sql.query(query, values, (err, res) => {
      if (err) {
        console.log("error: ", err);
        // result(null, err);
        return;
      }
      response.send(res);
      // result(null, res);
    });
  };


  Categoria.create = (newCategoria, response) => {
    sql.query("INSERT INTO categoria SET ?", newCategoria, (err, res) => {
      if (err) {
        console.log("error: ", err);
        // result(err, null);
        return;
      }
  
      console.log("created categoria: ", { id: res.insertId, ...newCategoria });
      response.send({ id: res.insertId, ...newCategoria });
      // result(null, { id: res.insertId, ...newCategoria });
    });
  };

  Categoria.remove = async (id, response) => {
    let query = "SELECT * FROM pics WHERE idCategoria = ?"
    let values = [id];

    let statusErro = 400
    let valueReturnErro = {message: "CATEGORIA ASSOCIADA A POST EXISTENTE"}

    let valueReturn = {message: "SUCCESS"}
    let status = 200

    sql.query(query, values, (error, resultado) =>{
      if (resultado.length>0){ 
        response.status(statusErro).send(valueReturnErro)
      } else {

        sql.query("DELETE FROM categoria WHERE id = ?", id, (err, res) => {
          if (err) {
            console.log("error: ", err);
            // result(null, err);
            status = 500;
          }
      
          if (res.affectedRows == 0) {
            // not found Pic with the id
            valueReturn = {message: "Id não encontrado"};
            status = 500;
          }
      
          console.log("deleted categoria with id: ", id);
          response.status(status).send(valueReturn);
          // result(null, res);
        });
      }
    })


  };
  
  
  module.exports = Categoria;